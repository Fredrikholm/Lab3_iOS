//
//  ViewController.swift
//  RunkeeperCloneSwift
//
//  Created by Fredrik Holm on 2017-11-13.
//  Copyright © 2017 Fredrik Holm. All rights reserved.
//

import UIKit
import MapKit
import AVFoundation
import AudioToolbox

let startLat = 57.79900731
let startLon = 14.14960741

class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    var player: AVAudioPlayer?
    var Applause_o_meter: Double = 1000
    var distance: Double = 0
    var locationManager: CLLocationManager!
    var startTime: Double = 0
    var time: Double = 0
    weak var timer: Timer?
    
    var clPrevPoint = CLLocation(latitude: startLat, longitude: startLon), clCurrPoint = CLLocation(latitude: startLat, longitude: startLon)
    
    override func viewDidAppear(_ animated: Bool) {
        startTime = Date().timeIntervalSinceReferenceDate
        timer = Timer.scheduledTimer(timeInterval: 0.05,
                                     target: self,
                                     selector: #selector(advanceTimer(timer:)),
                                     userInfo: nil,
                                     repeats: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.mapType = MKMapType(rawValue: 0)!
        mapView.userTrackingMode = MKUserTrackingMode(rawValue: 2)!
        
        let status = CLLocationManager.authorizationStatus()
        if status == .notDetermined || status == .denied || status == .authorizedWhenInUse { // redundant?
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
        }
        locationManager.startUpdatingLocation()
        locationManager.startUpdatingHeading()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let startPoint = CLLocationCoordinate2D(latitude: (locations.first?.coordinate.latitude)!, longitude: (locations.first?.coordinate.longitude)!)
        let currentPoint = CLLocationCoordinate2D(latitude: (locations.last?.coordinate.latitude)!, longitude: (locations.last?.coordinate.longitude)!)
        var area = [startPoint, currentPoint]
        let polyline = MKPolyline(coordinates: &area, count: area.count)
        mapView.add(polyline)
    
        if distance != 0 {
            clPrevPoint = clCurrPoint
        }
    
        let clStartPoint = CLLocation(latitude: startPoint.latitude, longitude: startPoint.longitude)
        clCurrPoint = CLLocation(latitude: currentPoint.latitude, longitude: currentPoint.longitude)
        print("\n\n ", clStartPoint)
        print("\n\n ", clCurrPoint)

        distance += clCurrPoint.distance(from: clPrevPoint)

        if Applause_o_meter <= distance {
            playSound()
            Applause_o_meter += 1000
        }
        
        let distString = String(format: "%.1f", distance)
        distanceLabel.text = "Distance(m): \(distString)"
     
    }
    
    func mapView(_ mapView: MKMapView!, rendererFor overlay: MKOverlay!) -> MKOverlayRenderer! {
        if overlay is MKPolyline {
            let pr = MKPolylineRenderer(overlay: overlay)
            pr.strokeColor = UIColor.red
            pr.lineWidth = 2
            return pr
        }
        return nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func advanceTimer(timer: Timer) {
        time = Date().timeIntervalSinceReferenceDate - startTime
        let (h,m,s) = secondsToHMS(seconds: Int(time))
        timeLabel.text = ("\(h):\(m):\(s)")
    }
    func secondsToHMS ( seconds: Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    func playSound() {
        let path = Bundle.main.path(forResource: "applause6", ofType: "mp3")
        let url = URL(fileURLWithPath: path!)
        
        do {
            let sound = try AVAudioPlayer(contentsOf: url)
            self.player = sound
            player?.numberOfLoops = 1
            player?.prepareToPlay()
            player?.play()
        } catch {
            print("error playing sound")
        }
    }
}

